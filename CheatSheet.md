Command Line Cheatsheet.
========================

Note: Linux is case-sensitive.

Open 'Terminal' application (under 'Accessories').

'dir' -- gives you a list of what is in the DIRectory.

'cd'+' ./DIRECTORY_NAME' -- Change Directory to DIRECTORY_NAME

'cd ~' -- change directory to root.

'cd ..' -- move up (/back) a directory.

TAB to autocomplete

 to run a python file, type 'python3' + FILE_NAME.py
 
 run 'BookScanner.py' to get book data from online for books with ISBNs.
 - note: it will crash if you're not online.
 - it will print the data into a new .csv file with a name that includes the date-time.


Git
===
https://xkcd.com/1597/

'git status' -- tells you whether you are up-to-date or not.

'git add ' + FILENAME -- adds the FILE that is not up-to-date.
'git add *' -- add everything.

git commit -m 'MESSAGE TO EXPLAIN' -- commit added changes to saved version.

git log -- shows record of all saved versions of filesystem.

git checkout 'HEXADECIMAL MARKERPOINT' -- go to previous saved point in log.

git checkout master -- return to most recent point.

git push -- push committed changes up to cloud backup. (you will need sign-in details. username: treeoflife. password: topsecretpassword.


TreeOfLife.org.uk
=================

It is a self-hosted WordPress site.

To log-in, go to: treeoflife.org.uk/wp-admin
Username: books@treeoflife.org.uk. Password: topsecretpassword.

Go to 'TablePress' Plug-in on Left. Export Table, edit, then Import (Updated) Table (choose 'Replace Existing Table').



Book Cataloguing To Do List
===========================

1. Finish cataloguing unbarcoded books in B1. (List of shelves with unbarcoded books is in ./stocklist/shelves.geany).
2. Finish cataloguing unbarcoded books in B2.
3. Integrate all catalogued books into Complete.csv spreadsheet.

+ Keep website spreadsheet current.
+ Keep on top of book donations -- scan barcoded for data, then sort by publisher into b1; put old hardbacks into b2 by publisher.
+ Add upstairs new stock to master Complete.csv.

++ Help design full-stack web-app to make updating book catalogue seamless.


Play with PointOfSale WebApp
============================

It is all in the 'bookpos' folder.

'python3 run.py'


Learn to Code
=============

freecodecamp.org -- Simple step-by-step teaching and exercises.
gitlab.org -- Sign up for your own git repository account, and try setting up a free webpage or website.
Learn Python.
Learn Javascript.
Learn HTML and CSS.

Geany
=====

f4 -- opens command terminal inside geany.
f2 -- focuses cursor in text editor inside geany.
Ctrl-E -- toggles whether code is live or 'commented'.
